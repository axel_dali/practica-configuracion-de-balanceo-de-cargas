# Práctica | Configuración de balanceo de cargas

Ejemplo de balanceo de cargas utilizando Docker Compose y la app de [@luis190991](https://gitlab.com/luis190991/balancingapp)

## Autor

- Axel Dalí Gomez Morales       ***329881***
    - [GitLab](https://gitlab.com/axel_dali)
